/* File implementing math-related code for matrixmangler
 * By: John Jekel (2021)
*/

#include "matrixmanip.h"

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <math.h>

void matrixInit(matrix_t* matrix, uint32_t rows, uint32_t columns)
{
    assert(rows);
    assert(columns);

    //Allocate room for the solution part of the augmented matrix
    matrix->solution = (double*)(malloc(rows * sizeof(double)));
    assert(matrix->solution);

    //Allocate room for the pointers to each row
    matrix->rowPtrs = (double**)(malloc(rows * sizeof(double*)));
    assert(matrix->rowPtrs);

    //Allocate room for the coefficient matrix (each row)
    for (uint32_t i = 0; i < rows; ++i)
    {
        matrix->rowPtrs[i] = (double*)(malloc(columns * sizeof(double)));
        assert(matrix->rowPtrs[i]);
    }

    matrix->rows = rows;
    matrix->columns = columns;
}

void matrixDeinit(matrix_t* matrix)
{
    assert(matrix->rows);
    assert(matrix->columns);

    //Free the solution part of the matrix
    assert(matrix->solution);
    free(matrix->solution);

    //Free each row
    for (uint32_t i = 0; i < matrix->rows; ++i)
    {
        assert(matrix->rowPtrs[i]);
        free(matrix->rowPtrs[i]);
    }

    //Free the pointers to each row
    free(matrix->rowPtrs);

    matrix->rows = 0;
    matrix->columns = 0;
}

void matrixPrint(matrix_t* matrix)
{
    assert(matrix);

    //Top corners of matrix
    fputs("┌ ", stdout);
    for (uint32_t i = 0; i < matrix->rows * 8; ++i)
        putchar(' ');
    puts("|        ┐");

    for (uint32_t i = 0; i < matrix->rows; ++i)
    {
        fputs("| ", stdout);

        for (uint32_t j = 0; j < matrix->columns; ++j)
            printf("% 4.3f  ", matrix->rowPtrs[i][j]);

        printf("| % 4.3f |\n", matrix->solution[i]);
    }

    //Bottom corners of matrix
    fputs("└ ", stdout);
    for (uint32_t i = 0; i < matrix->rows * 8; ++i)
        putchar(' ');
    puts("|        ┘");
}

void matrixRowMultiply(matrix_t* matrix, double factor, uint32_t row)
{
    assert(factor);

    //Multiply each element of the row by a factor
    for (uint32_t i = 0; i < matrix->columns; ++i)
        matrix->rowPtrs[row][i] *= factor;

    //Do the same for the solution
    matrix->solution[row] *= factor;

    matrixPrint(matrix);//TESTING
}

void matrixRowAddScalarMultiple(matrix_t* matrix, uint32_t row1, double factor, uint32_t row2)
{
    assert(row1 != row2);

    //Add scalar multiple of each of row2 to row2
    for (uint32_t i = 0; i < matrix->columns; ++i)
        matrix->rowPtrs[row1][i] += factor * matrix->rowPtrs[row2][i];

    //Do the same for the solution
    matrix->solution[row1] += factor * matrix->solution[row2];

    matrixPrint(matrix);//TESTING
}

void matrixRowSwap(matrix_t* matrix, uint32_t row1, uint32_t row2)
{
    assert(row1 != row2);

    //Swap row pointers
    double* tempPtr = matrix->rowPtrs[row1];
    matrix->rowPtrs[row1] = matrix->rowPtrs[row2];
    matrix->rowPtrs[row2] = tempPtr;

    //Swap solution values
    double temp = matrix->solution[row1];
    matrix->solution[row1] = matrix->solution[row2];
    matrix->solution[row2] = temp;

    matrixPrint(matrix);//TESTING
}

void matrixToREF(matrix_t* matrix)
{
    //In part thanks to https://en.wikipedia.org/wiki/Gaussian_elimination

    //Get matrix to REF
    for (uint32_t i = 0; i < matrix->columns; ++i)//i is also the pivot
    {
        //Identify pivot for the column
        uint32_t pivot = -1;
        double currentBiggest = matrix->rowPtrs[0][i];
        for (uint32_t j = 0; j < matrix->rows; ++j)
        {
            double absoluteValue = fabs(matrix->rowPtrs[j][i]);
            if (absoluteValue >= currentBiggest)
            {
                pivot = j;
                currentBiggest = absoluteValue;
            }
        }

        //If there's no leading entry, just continue on
        if (pivot == -1)//FIXME this dosn't work properly
            continue;

        //Swap it to where it should be if it is not there already
        if (pivot != i)
            matrixRowSwap(matrix, pivot, i);

        //Make the leading value for the row == 1
        matrixRowMultiply(matrix, 1.0/matrix->rowPtrs[i][i], i);

        //Make values in the columns below 0
        for (uint32_t j = i + 1; j < matrix->rows; ++j)
        {
            matrixRowAddScalarMultiple(matrix, j, -matrix->rowPtrs[j][i], i);
        }
    }
}

void matrixToRREF(matrix_t* matrix)
{
    matrixToREF(matrix);

    //Now we just need to get the matrix to RREF
    for (int32_t i = matrix->columns; i >= 0; --i)
    {

    }
}
